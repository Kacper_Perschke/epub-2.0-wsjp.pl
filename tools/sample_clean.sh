#!/bin/sh

SRC_DIR=src
TRG_DIR=entries

produce_entry() {
    sed \
        -e 's/^<html class=" js .*$/<html>/' \
        -e '/<meta name="\(viewport\)\|\(description\)\|\(robots\)"/d' \
        -e "s|<title> </title>|<title>$name</title>|" \
        -e '/<link .*rel="stylesheet"/d' \
        -e '/<script src="/d' \
        -e '/<\/\{0,1\}main>/d'\
        -e 's|<a href="javascript:window.print()" class="button_right print">DRUKUJ</a>||' \
        -e '/^<footer class=.*$/d' \
        $raw \
    | tidy -utf8 -i -w 32767 --tidy-mark no \
    | sed \
        -e 's|<div class="[a-z_]*"></div>||g' \
        -e '/^  *$/d' \
        -e '/^$/d' \
    > "$TRG_DIR"/"$name".html
}


for raw in "$SRC_DIR"/wsjp-*.html
do
    name=`grep '<h1>.*</h1>.*DRUKUJ' $raw | sed -e's/^.*<h1>//' -e's/<\/h1>.*$//' -e's/^  *//' -e's/  *$//'`
    if [ -n "$name" ];
    then
        echo $raw $name
        produce_entry
    fi
done
