# Attempt to convert the data published as [Wielki Słownik Języka Polskiego](http://wsjp.pl) in to the [Epub 2.0 format](https://en.wikipedia.org/wiki/EPUB).

You're encouraged to write your suugestions in the [issue tracker](https://bitbucket.org/Kacper_Perschke/epub-2.0-wsjp.pl/issues).
